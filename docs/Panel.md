# Panel #

Used to group a collection of components together.

## Usage ###

`Panel(children)`

### Arguments ###

children
:   Child components or mithril vnodes. Can be written as splat arguments.


## Examples ##

### Panel ###

```javascript
import Panel from 'substance/Panel';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      Panel('A lot of content is contained within this panel'),
      StyleManager()
    ];
  }
});
```
