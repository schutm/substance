# AppBar #

The `AppBar` provides a standard space in the app for default elements contained at a fixed position. By providing
default elements in a consistent place user will quickly understand how to use the app. The `AppBar` should be used to:

* Indicate the user's location in the app
* Provide important actions and actions which shouls be always available
* Support navigation

## Usage ##

`AppBar(children)`

### Arguments ###

children
:    Child components or mithril vnodes. Can be written as splat arguments.

### Theming ###

backgroundColor
:   Sets the background color of the AppBar.

    Defaults to `palette.primary1Color`.


textColor
:   Sets the foreground color / text color used by default in the AppBar.

    Defaults to `palette.alternateTextColor`.


height
:   Set the height of the AppBar, based on the size of the device.

    Defaults to `{ small: '48px', medium: '56px', large: '64px' }`.


textSize
:   Set the size of text used by default in the AppBar.

    Defaults to `'20px'`.


## Examples ##

### Appbar with title ###

```javascript
import AppBar from 'substance/AppBar';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
	view(_vnode) {
		return [
			AppBar('AppBar Example'),
			StyleManager()
		];
	}
});
```
