import { Button, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('Button HTML structure renders correctly with build-in effect', async () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(Button)('test');

	expect(domNode).toMatchSnapshot();
	await dispatch.mouseEvent(domNode, 'mousedown');
	expect(domNode).toMatchSnapshot();
});
