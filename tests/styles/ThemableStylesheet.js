import ThemableStylesheet from '../../src/styles/ThemableStylesheet';

const stylesheet1 = (theme) => `
  .stylesheet {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const stylesheet2 = (theme) => `
  .stylesheet {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const themeA = {
	backgroundColor: 'red',
	color: 'white'
};

const themeB = {
	backgroundColor: 'red',
	color: 'white'
};

describe('ThemableStylesheet CSS structure', () => {
	const themableStylesheet1 = new ThemableStylesheet(stylesheet1);
	const result = themableStylesheet1.getThemedStylesheet(themeA);

	it('renders correctly', () => {
		expect(result.unscopedCss).toMatchSnapshot();
	});
});

describe('ThemableStylesheet construction', () => {
	it('returns the same ThemableStylesheet for the same stylesheet', () => {
		expect(new ThemableStylesheet(stylesheet1)).toBe(new ThemableStylesheet(stylesheet1));
	});

	it('returns the different ThemableStylesheet for different stylesheets', () => {
		expect(new ThemableStylesheet(stylesheet1)).not.toBe(new ThemableStylesheet(stylesheet2));
	});
});

describe('ThemableStylesheet instantiation', () => {
	const themableStylesheet1 = new ThemableStylesheet(stylesheet1);
	const themableStylesheet2 = new ThemableStylesheet(stylesheet2);

	it('returns the same ThemedStylesheet for the same stylesheet and theme', () => {
		expect(themableStylesheet1.getThemedStylesheet(themeA)).toBe(themableStylesheet1.getThemedStylesheet(themeA));
	});

	it('returns different ThemedStylesheets for the same stylesheet and different themes', () => {
		expect(themableStylesheet1.getThemedStylesheet(themeA)).not.toBe(themableStylesheet1.getThemedStylesheet(themeB));
	});

	it('returns different ThemedStylesheets for different stylesheets and the same theme', () => {
		expect(themableStylesheet1.getThemedStylesheet(themeA)).not.toBe(themableStylesheet2.getThemedStylesheet(themeA));
	});

	it('returns different ThemedStylesheets for different stylesheets and themes', () => {
		expect(themableStylesheet1.getThemedStylesheet(themeA)).not.toBe(themableStylesheet2.getThemedStylesheet(themeB));
	});
});
