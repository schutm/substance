export const themeLight = {
	palette: {
		primary1Color: '#fff',
		primaryTextColor: '#000',
		white: '#fff',
		dark: '#000'
	}
};
