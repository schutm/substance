# Icon #

A quickly comprehensible symbol in order to help the user navigate. An `Icon` is commonly used as content of a
[`Button`](Button) or [`ToolButton`](ToolButton).
The icons are taken from [http://google.github.io/material-design-icons](http://google.github.io/material-design-icons).
For a complete list see [https://www.google.com/design/icons](https://www.google.com/design/icons).

## Usage ##

`Icon(ligature)`

### Arguments ###

ligature
:   The icon to show.


## Examples ##

### Some icons ###

```javascript
import Icon from 'substance/Icon';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      Icon('menu'),
      Icon('delete'),
      Icon('favorite'),
      Icon('more_vert'),
      Icon('more_horiz'),
      Icon('star_border'),
      StyleManager()
    ];
  }
});
```
