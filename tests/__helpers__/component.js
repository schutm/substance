import m from 'mithril/mithril';

global.m = m;

if (!global.requestAnimationFrame) {
	global.requestAnimationFrame = callback => {
		const hr = process.hrtime();
		const hrInNano = hr[0] * 1e9 + hr[1];
		const hrInMicro = hrInNano / 1e6;

		return global.setTimeout(callback, 0, hrInMicro);
	};
}

function waitForAnimationFrameToPass() {
	return new Promise((resolve) => requestAnimationFrame(() => {
		m.redraw.sync();
		resolve();
	}));
}

export let dispatch = {
	mouseEvent(domNode, type) {
		const evt = document.createEvent('MouseEvents');
		evt.initMouseEvent(type, true, true, window, 1, 97, 123, 37, 33,
		                   false, false, false, false, 0, null);
		domNode.dispatchEvent(evt);

		return waitForAnimationFrameToPass();
	},

	transitionEvent(domNode, type, property) {
		const evt = document.createEvent('event');
		evt.initEvent(type, true, false);
		evt.propertyName = property;
		domNode.dispatchEvent(evt);

		return waitForAnimationFrameToPass();
	}
};

export function createComponent(component) {
	return (...args) => {
		let vnode;

		m.mount(window.document.body, {
			view() {
				vnode = component(...args);

				return vnode;
			}
		});

		return {
			component: vnode,
			domNode: vnode.dom
		};
	};
}

export function createWrappedComponent(component) {
	return (...args) => {
		let vnode;

		m.mount(window.document.body, {
			view() {
				vnode = component(...args);

				return m('div', vnode);
			}
		});

		return {
			component: vnode,
			domNode: vnode.dom
		};
	};
}
