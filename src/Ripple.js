import { withStyles } from './styles/withStyles';
import { fade, contrastColor } from './utils/colorManipulator';

const OPACITY = 0.08;
const SPEED = {
	pressed: { radius: '1100ms', opacity: '900ms' },
	released: { radius: '275ms', opacity: '900ms' }
};

const ANIMATION = { curveLinearOutSlowIn: 'cubic-bezier(0, 0, 0.2, 1)' };

const stylesheet = (theme) => `
	.ripple {
		position: absolute;
		left: 0;
		top: 0;

		pointer-events: none;
		border-radius: 50%;
		background: ${theme.rippleColor};

		will-change: transform, width, opacity;
		transition: none;
		width: 0;
		opacity: 1;
	}

	.ripple:before {
		content: "";
		padding-top: 100%;
		display: block;
	}`;

const defaultTheme = ({ palette }) => {
	return {
		rippleColor: fade(contrastColor(palette.primaryTextColor, palette.white, palette.dark), OPACITY)
	};
};

function rippleSize(hotspot, domNode) {
	/* eslint-disable no-magic-numbers */
	const dimensions = domNode.getBoundingClientRect();
	const centerX = dimensions.width / 2;
	const centerY = dimensions.height / 2;

	const scale = 1.0 + Math.max(
		 Math.abs(centerX - hotspot.x) / centerX,
		 Math.abs(centerY - hotspot.y) / centerY
	);

	return Math.ceil(Math.sqrt((dimensions.width ** 2) + (dimensions.height ** 2)) * scale);
	/* eslint-enable no-magic-numbers */
}

/* eslint-disable no-param-reassign */
function speed(speeds, domNode) {
	domNode.style.transition = `width ${speeds.radius} ${ANIMATION.curveLinearOutSlowIn},
	                            opacity ${speeds.opacity} ${ANIMATION.curveLinearOutSlowIn}`;
}
/* eslint-enable no-param-reassign */

function eventToPromise(emitter, type) {
	return new Promise((resolve) => {
		function listener(ev) {
			if (ev.propertyName === 'opacity') {
				resolve();
			}
		}

		// eslint-disable-next-line no-useless-call
		emitter.addEventListener.call(emitter, type, listener);
	});
}

export class Ripple {
	oncreate(_styles, vnode) {
		const hotspot = vnode.attrs.hotspot;
		const size = rippleSize(hotspot, vnode.dom.parentNode);

		this.animationsFinished = eventToPromise(vnode.dom, 'transitionend');

		speed(SPEED.pressed, vnode.dom);
		/* eslint-disable no-param-reassign */
		vnode.dom.style.transform = `translate3d(-50%, -50%, 0) translate3d(${hotspot.x}px, ${hotspot.y}px, 0)`;
		vnode.dom.style.width = `${size}px`;
		/* eslint-enable no-param-reassign */
	}

	onbeforeremove(_styles, vnode) {
		speed(SPEED.released, vnode.dom);
		// eslint-disable-next-line no-param-reassign
		vnode.dom.style.opacity = 0;

		return this.animationsFinished;
	}

	view(styles, _vnode) {
		return m(`span.${styles.ripple}`);
	}

	static createComponent(theme) {
		return withStyles(stylesheet, theme)(this);
	}
}

const component = Ripple.createComponent(defaultTheme);
export default function RippleComponent(...args) {
	return m(component, ...args);
}
