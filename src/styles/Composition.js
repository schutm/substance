export default class Composition {
	constructor(scopedName, unscopedName, prefix) {
		this.scopedName = scopedName;
		this.unscopedName = unscopedName;
		this.prefix = prefix;
	}

	get selector() {
		return this.prefix + this.scopedName;
	}

	get unscopedSelector() {
		return this.prefix + this.unscopedName;
	}

	toString() {
		return this.scopedName;
	}
}
