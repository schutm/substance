import { ThemeProvider } from '../../src';

import { withStyles } from '../../src/styles/withStyles';

import m from 'mithril/mithril';
global.m = m;

const defaultTheme = (theme) => {
	return { name: theme.name };
};

const themeA = { name: 'a' };
const themeB = { name: 'b' };

const stylesheetTest1 = (theme) => `.test { name: ${theme.name} }`;

const testComponent = withStyles(stylesheetTest1, defaultTheme)(class Test1 {
	view(styles, vnode) { return m(`.${styles.test}`, vnode.children); }
});

function createComponent() {
	m.mount(window.document.body, {
		view() {
			return m('div',
			         ThemeProvider({ theme: themeA },
			           m(testComponent, 'themeA'),
			           ThemeProvider({ theme: themeB },
			             m(testComponent, 'themeB')
			           ),
			           m(testComponent, 'themeA again')
			         )
			);
		}
	});
}

test('ThemeProvider HTML structure renders correctly', () => {
	createComponent();

	expect(window.document.documentElement).toMatchSnapshot();
});
