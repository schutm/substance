import { wrapComponent } from '../../src/styles/m-utils';

class Component {
	view(arg) { return `(view) arg: ${arg}`; }
	unknown(arg) { return `(unknown) arg: ${arg}`; }
}

describe('Wrapped component', () => {
	const WrappedComponent = wrapComponent(Component);

	it("has it's name prefixed with Wrapped", () => {
		expect(WrappedComponent.name).toBe('WrappedComponent');
	});
});

describe('Instantiated wrapped component without explicit wrapping', () => {
	const WrappedComponent = wrapComponent(Component, (fn) => (...args) => `[wrapped] ${fn(...args)}`);
	const wrappedComponent = new WrappedComponent();

	it('mithril methods are wrapped', () => {
		expect(wrappedComponent.view('test')).toBe('[wrapped] (view) arg: test');
	});

	it('non-mithril methods are not wrapped', () => {
		expect(wrappedComponent.unknown('test')).toBe('(unknown) arg: test');
	});

	it('non-existing mithril methods are not created', () => {
		expect(wrappedComponent.oninit).toBeUndefined();
	});
});

describe('Instantiated wrapped component with explicit wrapping', () => {
	const WrappedComponent = wrapComponent(
		Component,
		(fn) => (...args) => `[wrapped] ${fn(...args)}`,
		['oninit', 'unknown']
	);
	const wrappedComponent = new WrappedComponent();

	it('non-passed method not to be wrapped', () => {
		expect(wrappedComponent.view('test')).toBe('(view) arg: test');
	});

	it('passed method to be wrapped', () => {
		expect(wrappedComponent.unknown('test')).toBe('[wrapped] (unknown) arg: test');
	});

	it('non-existing methods to not be created', () => {
		expect(wrappedComponent.oninit).toBeUndefined();
	});
});
