import { themedStylesheets } from './styles/withStyles';

export default function StyleManager() {
	return m({
		view(_vnode) {
			return themedStylesheets().map((stylesheet) => m('style', stylesheet.css));
		}
	});
}
