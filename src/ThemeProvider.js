import { activateTheme, deactivateTheme } from './styles/withStyles';

const componentEvents = ['oninit',
	                       'oncreate',
	                       'onupdate',
	                       'onbeforeremove',
	                       'onremove',
	                       'onbeforeupdate',
	                       'view'];

function createThemeActivator(theme) {
	return Object.assign({}, ...componentEvents.map((ev) => {
		return {
			[ev]() { activateTheme(theme); }
		};
	}));
}

function createThemeDeactivator() {
	return Object.assign({}, ...componentEvents.map((ev) => {
		return {
			[ev]() { deactivateTheme(); }
		};
	}));
}

export default function ThemeProviderComponent({ theme }, ...children) {
	const activate = createThemeActivator(theme);
	const deactivate = createThemeDeactivator();

	return [
		m.fragment(activate, []),
		...children,
		m.fragment(deactivate, [])
	];
}
