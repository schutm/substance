import { Icon } from '../../src';

describe('The path of the icon font', () => {
	it('has an empty default value', () => {
		expect(Icon.getFontPath()).toBe('');
	});

	it('has sets and gets the same value', () => {
		const path = 'path/to/fonticon/';

		Icon.setFontPath(path);
		expect(Icon.getFontPath()).toBe(path);
	});
});
