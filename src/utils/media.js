function minWidth(mediaqueries) {
	const min = Math.min(...mediaqueries.map((currentValue) => currentValue.minWidth));

	return Number.isInteger(min) ? `(min-width: ${min}px)` : null;
}

function maxWidth(mediaqueries) {
	const max = Math.max(...mediaqueries.map((currentValue) => currentValue.maxWidth));

	return Number.isInteger(max) ? `(max-width: ${max}px)` : null;
}

export function media(...mediaqueries) {
	const minWidthCondition = minWidth(mediaqueries);
	const maxWidthCondition = maxWidth(mediaqueries);
	const query = [minWidthCondition, maxWidthCondition].filter((condition) => !!condition)
	                                                    .join(' and ');

	return `@media ${query}`;
}

export const viewport = {
	xs: { minWidth: 0, maxWidth: 576 },
	sm: { minWidth: 577, maxWidth: 768 },
	md: { minWidth: 769, maxWidth: 992 },
	lg: { minWidth: 993, maxWidth: 1200 },
	xl: { minWidth: 1201 }
};
