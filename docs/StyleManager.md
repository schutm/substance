# StyleManager #

Emits the styles used by the elements. It is prefered to always include this element as the last element in a view.

## Usage ##

`StyleManager()`
