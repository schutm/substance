jQuery(function() {
	const script = document.currentScript || $(document.getElementsByTagName("script")).slice(-1)[0];
	const src = script.src;

	$('code.javascript').each(function() {
		var $codeElement = $(this);
		var $demo = $('<iframe class="demo"></iframe><div class="example-code-title">Source code</div>');
		var code = $codeElement.text()
		                       .split('\n')
		                       .filter((line) => !line.startsWith('import'))
		                       .join('\n');

		$codeElement.before($demo);

		var document = $demo.get(0).contentDocument;
		document.open();
		document.write(
			'<!DOCTYPE html>' +
			'<html>' +
			'   <head>' +
			'     <script src="' + src + '/../../vendor/mithril.min.js"></script>' +
			'     <script src="' + src + '/../../vendor/substance.iife.min.js"></script>' +
			'     <script>Object.keys(substance).forEach((key) => window[key] = substance[key]);</script>' +
			'   </head>' +
			'   <body>' +
			'     <script>' + code + '</script>' +
			'   </body>' +
			'</html>');
			document.close();
	});
});
