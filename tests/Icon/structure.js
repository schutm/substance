import { Icon, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

beforeAll(() => {
	document.head.innerHTML = '<script src="path/to/script.js">';
});

test('Icon HTML structure renders correctly', () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(Icon)('menu');

	expect(domNode).toMatchSnapshot();
});
