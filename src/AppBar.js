import { withStyles } from './styles/withStyles';
import { media, viewport } from './utils/media';

import Layout from './Layout';

const stylesheet = (theme) => `
	.appbar {
		width: 100%;
		position: fixed;
		top: 0;
		left: 0;
		z-index: 999;
		background-color: ${theme.backgroundColor};
		color: ${theme.textColor};
		font-size: ${theme.textSize};
	}

	${media(viewport.xs, viewport.sm)} {
		.appbar {
			height: ${theme.height.small};
		}
	}

	${media(viewport.md)} {
		.appbar {
			height: ${theme.height.medium};
		}
	}

	${media(viewport.lg, viewport.xl)} {
		.appbar {
			height: ${theme.height.large};
		}
	}`;

const defaultTheme = ({ palette }) => {
	return {
		backgroundColor: palette.primary1Color,
		textColor: palette.alternateTextColor,
		height: { small: '48px', medium: '56px', large: '64px' },
		textSize: '20px'
	};
};

export class AppBar {
	view(styles, vnode) {
		const attrStyle = Object.assign({}, vnode.attrs.style, Layout.GridLayout);
		const attrs = Object.assign({}, vnode.attrs, { style: attrStyle });

		return m(`.${styles.appbar}`, attrs, vnode.children);
	}

	static createComponent(theme) {
		return withStyles(stylesheet, theme)(this);
	}
}

const component = AppBar.createComponent(defaultTheme);
export default function AppBarComponent(...args) {
	return m(component, ...args);
}
