import { Button, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

let animationState;
class FakeAnimation {
	oncreate() { animationState = 'started'; }
	onbeforeremove() { animationState = 'ended'; }
	view() { return m(''); }
}

const eventListener = {
	callback(ev) { this.type = ev.type; }
};

test('Button events are emitted', async () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(Button)({
		animation: () => m(FakeAnimation),
		onmousedown: (ev) => eventListener.callback(ev),
		onmouseup: (ev) => eventListener.callback(ev),
		onmouseleave: (ev) => eventListener.callback(ev)
	}, 'test');

	expect(eventListener.type).toBeUndefined();

	await dispatch.mouseEvent(domNode, 'mousedown');
	expect(eventListener.type).toBe('mousedown');
	expect(animationState).toBe('started');

	await dispatch.mouseEvent(domNode, 'mouseup');
	expect(eventListener.type).toBe('mouseup');
	expect(animationState).toBe('ended');

	await dispatch.mouseEvent(domNode, 'mouseleave');
	expect(eventListener.type).toBe('mouseleave');
	expect(animationState).toBe('ended');
});
