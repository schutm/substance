import { wrapComponent } from './m-utils';
import ThemableStylesheet from './ThemableStylesheet';

const activatedThemes = [];
const themesPerComponent = {};
const usedThemedStylesheets = new Set();

function getComponentTheme(componentName, theme, defaultSkin) {
	if (!themesPerComponent[componentName]) {
		themesPerComponent[componentName] = new Map();
	}

	if (!themesPerComponent[componentName].has(theme)) {
		themesPerComponent[componentName].set(theme, defaultSkin(theme));
	}

	return themesPerComponent[componentName].get(theme);
}

function createThemeWrapper(componentName, themableStylesheet, defaultSkin) {
	return (fn) => {
		const theme = currentTheme();
		const componentTheme = getComponentTheme(componentName, theme, defaultSkin);
		const themedStylesheet = themableStylesheet.getThemedStylesheet(componentTheme);
		if (!usedThemedStylesheets.has(themedStylesheet)) {
			usedThemedStylesheets.add(themedStylesheet);
		}

		return (...args) => fn(themedStylesheet.classes, ...args);
	};
}

function currentTheme() {
	/* eslint-disable no-magic-numbers */
	return activatedThemes[0];
	/* eslint-enable */
}

export function withStyles(stylesheetFunction, defaultSkin) {
	const themableStylesheet = new ThemableStylesheet(stylesheetFunction);

	return (...args) => {
		/* eslint-disable no-magic-numbers */
		const component = args[1] || args[0];
		const componentName = typeof args[0] === 'string' ? args[0] : component.name;
		/* eslint-enable */
		const themeWrapper = createThemeWrapper(componentName, themableStylesheet, defaultSkin);

		return wrapComponent(component, themeWrapper);
	};
}

export function activateTheme(theme) {
	activatedThemes.unshift(theme);
}

export function deactivateTheme() {
	activatedThemes.shift();
}

export function themedStylesheets() {
	return Array.from(usedThemedStylesheets.values());
}
