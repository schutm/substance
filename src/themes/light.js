/* eslint-disable no-magic-numbers */

import {
	cyan500, cyan700,
	pinkA200,
	grey100, grey300, grey400, grey500,
	white, fullWhite, darkBlack, fullBlack
} from './colors';
import { fade } from '../utils/colorManipulator';

const theme = {
	fontFamily: 'Roboto, sans-serif',
	palette: {
		primary1Color: cyan500,
		primary2Color: cyan700,
		primary3Color: grey400,
		accent1Color: pinkA200,
		accent2Color: grey100,
		accent3Color: grey500,
		primaryTextColor: fade(darkBlack, 0.87),
		secondaryTextColor: fade(darkBlack, 0.54),
		disabledTextColor: fade(darkBlack, 0.38),
		alternateTextColor: white,
		canvasColor: white,
		borderColor: grey300,
		disabledColor: fade(darkBlack, 0.3),
		shadowColor: fullBlack,

		white: fullWhite,
		dark: fullBlack
	}
};

export default theme;
