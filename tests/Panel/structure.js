import { Panel, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('Panel HTML structure renders correctly', () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(Panel)('test');

	expect(domNode).toMatchSnapshot();
});
