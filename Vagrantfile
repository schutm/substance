project     = ENV.fetch('PROJECT', File.basename(File.dirname(__FILE__)))
base_dir    = File.expand_path(File.dirname(__FILE__) + '/../..')

$HOST = {
	'base_dir'    => ENV.fetch('BASEDIR', base_dir),
	'source_dir'  => ENV.fetch('SOURCEDIR', File.dirname(__FILE__)),
	'output_dir'  => ENV.fetch('OUTPUTDIR', "#{base_dir}/Outputs/#{project}")
}

$GUEST = {
	'base_dir'    => '/project',
	'source_dir'  => "/project/sources"
}

def ensure_dir target
  unless File.directory?(target)
    FileUtils.mkdir_p(target)
  end
end

def provision root
  Dir.glob("#{root}/**/*.provisioner").sort.each do |f|
    puts "Including '#{f}'"
    Vagrant.configure(2) do |config|
      config.vm.provision "shell", path: f, env: { "SOURCEDIR" => $GUEST['source_dir']}
    end
  end
end

ensure_dir($HOST['output_dir'])

Vagrant.configure(2) do |config|
  # Box to work with
  config.vm.box = "debian/contrib-stretch64"
  config.vm.box_check_update = false

  # Network
  config.vm.network "forwarded_port", guest: 80, host_ip: "127.0.0.1", host: 8000, auto_correct: true

  # Shared folders.
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder $HOST['source_dir'], $GUEST['source_dir']

  # Make sure we don't get non-interactive shell problems
  config.ssh.shell = "bash"

  # Set owner of shared folder.
  config.vm.provision "shell", inline: "chown vagrant:vagrant #{$GUEST['base_dir']}"
end

provision($HOST['source_dir'])
