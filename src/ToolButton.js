import { Button, Appearance } from './Button';

const defaultTheme = ({ palette }) => {
	return {
		textColor: palette.alternateTextColor,
		backgroundColor: palette.primary1Color,
		height: { small: '100%', medium: '100%', large: '100%' },
		marginVertical: '0',
		paddingHorizontal: { small: '26px', medium: '16px', large: '26px' },
		borderRadius: '2px',
		textSize: { small: '13px', medium: '14px', large: '14px' }
	};
};

export {
	Appearance
};

export class ToolButton extends Button {
	view(styles, vnode) {
		const attrs = Object.assign({}, vnode.attrs, { type: Appearance.Type.Flat });
		const toolButtonVnode = Object.assign({}, vnode, { attrs });

		return super.view(styles, toolButtonVnode);
	}
}

const component = ToolButton.createComponent(defaultTheme);
export default function ToolButtonComponent(...args) {
	return m(component, ...args);
}
