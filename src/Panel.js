import { withStyles } from './styles/withStyles';

const stylesheet = (_theme) => ``;

const defaultTheme = ({ _palette }) => {
	return { };
};

export class Panel {
	view(_styles, vnode) {
		return m('div', vnode.attrs, vnode.children);
	}

	static createComponent(theme) {
		return withStyles(stylesheet, theme)(this);
	}
}

const component = Panel.createComponent(defaultTheme);
export default function PanelComponent(...args) {
	return m(component, ...args);
}
