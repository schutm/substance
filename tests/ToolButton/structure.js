import { ToolButton, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('ToolButton HTML structure renders correctly', () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(ToolButton)('test');

	expect(domNode).toMatchSnapshot();
});
