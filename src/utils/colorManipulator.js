/* eslint-disable no-magic-numbers */

/*
 * Color manipulations based on https://github.com/callemall/material-ui/
 */

const MODELS = {
	hsla: {
		parser: (h, s, l, a = 1) => [parseFloat(h), parseFloat(s), parseFloat(l), parseFloat(a)],
		generator: (h, s, l, a) => maybeWithAlpha('hsl', a, `${h}, ${s}%, ${l}%`),
		luminance: (_h, _s, l) => l / 100
	},
	rgba: {
		parser: (r, g, b, a = 1) => [parseFloat(r), parseFloat(g), parseFloat(b), parseFloat(a)],
		generator: (r, g, b, a) => maybeWithAlpha('rgb', a, `${r}, ${g}, ${b}`),
		// Formula: http://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
		luminance: (r, g, b) => (0.2126 * gamma22Fit(r))
		                          + (0.7152 * gamma22Fit(g)) + (0.0722 * gamma22Fit(b))
	}
};

function maybeWithAlpha(type, alpha, colorString) {
	const alphaPostfix = alpha < 1 ? 'a' : '';
	const alphaChannel = alpha < 1 ? `, ${alpha}` : '';

	return `${type}${alphaPostfix}(${colorString}${alphaChannel})`;
}

function clamp(value, min, max) {
	return Math.min(Math.max(value, min), max);
}

function convertColorToString(color) {
	const { model, channels } = color;

	return model.generator(...channels);
}

function extendHexColor(color) {
	const shorts = color.split('').slice(1);
	const extended = shorts.map((character) => character + character).join('');

	return `#${extended}`;
}

function convertHexToRGB(color) {
	const extendedColor = color.length === 4 ? extendHexColor(color) : color;

	const values = {
		r: parseInt(extendedColor.substr(1, 2), 16),
		g: parseInt(extendedColor.substr(3, 2), 16),
		b: parseInt(extendedColor.substr(5, 2), 16)
	};

	return `rgb(${values.r}, ${values.g}, ${values.b})`;
}

function decomposeColor(color) {
	if (color.charAt(0) === '#') {
		return decomposeColor(convertHexToRGB(color));
	}

	const startMarker = color.indexOf('(');
	const endMarker = color.indexOf(')');
	const type = color.substring(0, startMarker);
	const harmonizedType = Object.keys(MODELS).find((key) => key.indexOf(type) === 0);
	const model = MODELS[harmonizedType];

	const values = color.substring(startMarker + 1, endMarker).split(',');
	const channels = model.parser(...values);

	return { model, channels };
}

function gamma22Fit(channel) {
	const normalizedChannel = channel / 255;

	if (normalizedChannel < 0.03928) {
		return normalizedChannel / 12.92;
	}

	return ((normalizedChannel + 0.055) / 1.055) ** 2.4;
}

function getLuminance(color) {
	const decomposedColor = decomposeColor(color);
	const { model, channels } = decomposedColor;

	return model.luminance(...channels);
}

function getContrastRatio(foregroundColor, backgroundColor) {
	const luminanceA = getLuminance(foregroundColor);
	const luminanceB = getLuminance(backgroundColor);

	const maxLuminance = 0.05 + Math.max(luminanceA, luminanceB);
	const minLuminance = 0.05 + Math.min(luminanceA, luminanceB);
	const contrastRatio = maxLuminance / minLuminance;

	return contrastRatio;
}

/*
 * Set the absolute transparency of a color.
 * Any existing alpha values are overwritten.
 */
export function fade(color, alpha) {
	const decomposedColor = decomposeColor(color);
	decomposedColor.channels[3] = clamp(alpha, 0, 1);

	return convertColorToString(decomposedColor);
}

/*
 * Choose which of two colors should be used based on the contrast with the specified
 * color compared to black..
 * The threshold ranges from [0, 21]
 */
export function contrastColor(color, lightColor, darkColor, threshold = 7) {
	const contrastRatio = getContrastRatio(color, '#000');

	if (contrastRatio <= threshold) {
		return darkColor;
	}

	return lightColor;
}
