import { Ripple, activateTheme } from '../../src';

import { dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

let animate = true;
function createRippleComponent(...args) {
	let vnode;

	m.mount(window.document.body, {
		view() {
			vnode = m('div', { onmouseleave() { animate = false; } }, animate ? Ripple(...args) : null);

			return vnode;
		}
	});

	return {
		component: vnode,
		domNode: vnode.dom
	};
}

test('Ripple HTML structure renders effect correctly', async () => {
	activateTheme(themeLight);
	const {domNode} = createRippleComponent({ hotspot: { x: 20, y: 20 } });

	expect(domNode).toMatchSnapshot();

	await dispatch.mouseEvent(domNode, 'mouseleave');
	expect(domNode).toMatchSnapshot();
});
