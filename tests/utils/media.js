import { media, viewport } from '../../src/utils/media';

describe('And mediaquery', () => {
	it('has a minimum and maximum width if both defined', () => {
		expect(media(viewport.xs, viewport.lg)).toBe('@media (min-width: 0px) and (max-width: 1200px)');
	});

	it('has no maximum width if not defined', () => {
		expect(media(viewport.xs, viewport.xl)).toBe('@media (min-width: 0px)');
	});

	it('has no minimum width if not defined', () => {
		expect(media({ maxWidth: 0 }, viewport.lg)).toBe('@media (max-width: 1200px)');
	});
});
