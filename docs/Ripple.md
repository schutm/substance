# Ripple #

Shows a ripple effect covering the parent element the moment it is added to the parent.

## Usage ##

`Ripple(attributes)`

### Arguments ###

attributes
:   Additional attributes to style the `Ripple` and add events.

    hotspot
    :   An object with an x and y property identifying the hotspot (starting point) of the `Ripple`. E.g.
        `{x: 10, y: 15}`. A good idea is to feed the mouse event, which already has the `x` and `y` properties set.


## Examples ##

### Simple ripple effect ###

```javascript
import Ripple from 'substance/Ripple';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      m('div', { style: {
        background: 'red',
        width: '30px',
        height: '30px',
        position: 'relative',
        overflow: 'hidden' }
      }, showRipple ? Ripple({hotspot: {x: 10,  y: 15}}) : null),
      m('div', { style: {
        background: 'green',
        width: '30px',
        height: '30px',
        position: 'relative',
        overflow: 'hidden' }
      }, showRipple ? Ripple({hotspot: {x: 30,  y: 30}}) : null),
      m('div', { style: {
        background: 'blue',
        width: '120px',
        height: '30px',
        position: 'relative',
        overflow: 'hidden' }
      }, showRipple ? Ripple({hotspot: {x: 60,  y: 15}}) : null),
      StyleManager()
    ];
  }
});

var showRipple = false;
setInterval(() => {
  showRipple = !showRipple;
  m.redraw();
}, 2000);
```
