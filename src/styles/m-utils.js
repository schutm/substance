const componentEvents = ['oninit',
	                       'oncreate',
	                       'onupdate',
	                       'onbeforeremove',
	                       'onremove',
	                       'onbeforeupdate',
	                       'view'];

export function wrapComponent(component, wrapperFn, events = componentEvents) {
	class WrappedComponent extends component {
		static get name() { return `Wrapped${component.name}`; }
	}

	const wrappedMethods = events.map((ev) => (
		component.prototype[ev] ? { [ev](...args) {
			                            return (wrapperFn(component.prototype[ev].bind(this)))(...args);
			                        } }
		                        : null)).filter((ev) => !!ev);
	Object.assign(WrappedComponent.prototype, ...wrappedMethods);

	return WrappedComponent;
}
