# substance #

substance provides themable components for mithril. By design substance is biased towards app development, However it
is still possible to adjust the themes to ones liking. The principle of substance is that it should be able to use
components on any device, of any size.
