class Component {
	view(styles) { return styles.appbar.toString(); }
}

const stylesheet = (theme) => `
  .appbar {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const defaultSkin = ({ palette }) => {
	return {
		backgroundColor: palette.primary1Color,
		color: palette.alternateTextColor
	};
};

const defaultTheme = {
	palette: {
		primary1Color: '#800000',
		alternateTextColor: 'black'
	}
};

const secondTheme = {
	palette: {
		primary1Color: 'black',
		alternateTextColor: '#800000'
	}
};

beforeEach(() => {
  jest.resetModules();
});

describe('Styling', () => {
	it('support simple theming', () => {
		const { withStyles, activateTheme } = require('../../src/styles/withStyles');

		const StyledComponent = withStyles(stylesheet, defaultSkin)(Component);
		const styledComponent = new StyledComponent();

		activateTheme(defaultTheme);
		expect(styledComponent.view()).toBe('appbar_3xV7Ku');
	});

	it('supports explicitly named component', () => {
		const { withStyles, activateTheme } = require('../../src/styles/withStyles');

		const StyledComponent = withStyles(stylesheet, defaultSkin)('MyComponent', Component);
		const styledComponent = new StyledComponent();

		activateTheme(defaultTheme);
		expect(styledComponent.view()).toBe('appbar_3xV7Ku');
	});

	it('supports theme switching', () => {
		const { withStyles, activateTheme, deactivateTheme } = require('../../src/styles/withStyles');

		const StyledComponent = withStyles(stylesheet, defaultSkin)(Component);
		const styledComponent = new StyledComponent();

		activateTheme(defaultTheme);
		expect(styledComponent.view()).toBe('appbar_3xV7Ku');

		activateTheme(secondTheme);
		expect(styledComponent.view()).toBe('appbar_SbU6G');

		deactivateTheme();
		expect(styledComponent.view()).toBe('appbar_3xV7Ku');
	});

	it("doesn't repeat ThemedStylesheets", () => {
		const { withStyles, activateTheme, themedStylesheets } = require('../../src/styles/withStyles');

		const StyledComponent = withStyles(stylesheet, defaultSkin)(Component);
		const styledComponent = new StyledComponent();

		activateTheme(defaultTheme);
		styledComponent.view();
		styledComponent.view();

		// eslint-disable-next-line no-magic-numbers
		expect(themedStylesheets()).toHaveLength(1);
	});
});
