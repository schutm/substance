import { AppBar, activateTheme } from '../../src';

import { createComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('AppBar HTML structure renders correctly', () => {
	activateTheme(themeLight);
	const {domNode} = createComponent(AppBar)('test');

	expect(domNode).toMatchSnapshot();
});
