import ThemedStylesheet from './ThemedStylesheet';

const themableStylesheets = new Map();
export default class ThemableStylesheet {
	constructor(stylesheetFunction) {
		if (!themableStylesheets.has(stylesheetFunction)) {
			this.stylesheetFunction = stylesheetFunction;
			this.themedStylesheets = new Map();
			themableStylesheets.set(stylesheetFunction, this);
		}

		return themableStylesheets.get(stylesheetFunction);
	}

	getThemedStylesheet(theme) {
		if (!this.themedStylesheets.has(theme)) {
			const themedCss = this.stylesheetFunction(theme);
			this.themedStylesheets.set(theme, new ThemedStylesheet(themedCss));
		}

		return this.themedStylesheets.get(theme);
	}
}
