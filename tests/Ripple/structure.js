import { Ripple, activateTheme } from '../../src';

import { createWrappedComponent } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('Ripple HTML structure renders correctly', () => {
	activateTheme(themeLight);
	const {domNode} = createWrappedComponent(Ripple)({ hotspot: { x: 20, y: 20 } });

	expect(domNode).toMatchSnapshot();
});
