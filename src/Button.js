import { withStyles } from './styles/withStyles';
import { media, viewport } from './utils/media';
import { fade, contrastColor } from './utils/colorManipulator';

import Ripple from './Ripple';

const DARK_OPACITY = 0.12;
const LIGHT_OPACITY = 0.29;

const stylesheet = (theme) => `
	.button {
		appearance: none;
		user-select: none; /* would interfere with dragging */
		touch-action: manipulation;

		position: relative;
		overflow: hidden;
		z-index: 0;

		display: inline-block;
		margin: ${theme.marginVertical} 0;

		text-transform: uppercase;

		border-radius: ${theme.borderRadius};
		cursor: pointer;
		color: ${theme.textColor};
		background-color: transparent;
		background-image: none;  /* reset unusual Firefox-on-Android */

		text-align: center;
		vertical-align: middle;
		white-space: nowrap;
		font-family: inherit;
		letter-spacing: 0.03em;
		line-height: 1;
	}

	.button:hover,
	.button:focus,
	.button:active {
		outline: 0;
		text-decoration: none;
		background-color: ${theme.backgroundColor}
	}

	.flat {
		border: none;
	}

	${media(viewport.xs, viewport.sm)} {
		.button {
			padding: 0 ${theme.paddingHorizontal.small};
			height: ${theme.height.small};
			font-size: ${theme.textSize.small};
		}
	}

	${media(viewport.md)} {
		.button {
			padding: 0 ${theme.paddingHorizontal.medium};
			height: ${theme.height.medium};
			font-size: ${theme.textSize.medium};
		}
	}

	${media(viewport.lg, viewport.xl)} {
		.button {
			padding: 0 ${theme.paddingHorizontal.large};
			height: ${theme.height.large};
			font-size: ${theme.textSize.large};
		}
	}`;

const defaultTheme = ({ palette }) => {
	const backgroundColor = contrastColor(
		palette.primaryTextColor,
		fade(palette.primaryTextColor, DARK_OPACITY),
		fade(palette.primaryTextColor, LIGHT_OPACITY)
	);

	return {
		textColor: palette.primaryTextColor,
		backgroundColor,
		height: { small: '36px', medium: '31px', large: '55px' },
		marginVertical: '6px',
		paddingHorizontal: { small: '26px', medium: '16px', large: '26px' },
		borderRadius: '2px',
		borderColor: palette.primary1Color,
		textSize: { small: '13px', medium: '14px', large: '14px' }
	};
};

function animation(animationClass, args) {
	if (animationClass !== null && !!args) {
		return (animationClass || Ripple)(args);
	}

	return null;
}

export const Appearance = {
	Type: {
		// Action: 'action',
		Flat: 'flat'
		// Raised: 'raised'
	}
};

export class Button {
	view(styles, vnode) {
		const additionalStyles = styles(
			vnode.attrs.type || Appearance.Type.Flat,
			vnode.attrs.textColor
		);

		return m(`button.${styles.button} ${additionalStyles}`, {
			onmousedown: (ev) => {
				this.animationArgs = { hotspot: ev };

				return !!vnode.attrs.onmousedown && vnode.attrs.onmousedown(ev);
			},
			onmouseup: (ev) => {
				this.animationArgs = null;

				return !!vnode.attrs.onmouseup && vnode.attrs.onmouseup(ev);
			},
			onmouseleave: (ev) => {
				this.animationArgs = null;

				return !!vnode.attrs.onmouseleave && vnode.attrs.onmouseleave(ev);
			}
		}, animation(vnode.attrs.animation, this.animationArgs), ...vnode.children);
	}

	static createComponent(theme) {
		return withStyles(stylesheet, theme)(this);
	}
}

const component = Button.createComponent(defaultTheme);
export default function ButtonComponent(...args) {
	return m(component, ...args);
}
