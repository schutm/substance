# ToolButton #

A `ToolButton` is specialized form of a [`Button`](Button). It should be used in an [`AppBar`](AppBar) where a full
blown [`Button`](Button) is not desired. A `ToolButton` is generally more compressed in the space it takes up around
itself. Where a [`Button`](Button) provides some breething space a `ToolButton` doesn't provide this. The `ToolButton`'s
`Type` is always `Flat`.


## Usage ##

`ToolButton(attributes, children)`

### Arguments ###

attributes
:   Additional attributes to style the `ToolButton` and add events.

    animation
    :   The animation to apply if the `ToolButton` is clicked.

        Defaults to [`Ripple`](Ripple).

    textColor
    :   The color of the foreground text of the `ToolButton`.

        No default.

    *event*
    :   Any event as specified by the
        [HTML button element](https://w3c.github.io/html/sec-forms.html#the-button-element).

children
:   Child components or mithril vnodes. Can be written as splat arguments.

### Theming ###

backgroundColor
:   Sets the background color of the `ToolButton`.

    Defaults to `palette.primary1Color`.


borderColor
:   Set the color of the border of the `ToolButton`.

    Defaults to `palette.primary1Color`.


borderRadius
:   Specify the 'rounding' of the corners of the `ToolButton`.

Default to `'2px'`.


height
:   Set the height of the `ToolButton`, based on the size of the device.

    Defaults to `{ small: '100%', medium: '100%', large: '100%' }`.


marginVertical
:   The vertical space between the `ToolButton` and other elements.

Defaults to `'0'`.


paddingHorizontal
:   The horizontal space between the `ToolButton` content and the border of the `ToolButton`.

    Defaults to `{ small: '26px', medium: '16px', large: '26px' }`.


textColor
:   Sets the foreground color / text color used by default by the `ToolButton`.

    Defaults to `palette.alternateTextColor`.


textSize
:    Set the size of text used by default by the `ToolButton`.

     Defaults to `{ small: '13px', medium: '14px', large: '14px' }`.


## Examples ##

### A toolbutton ###

```javascript
import AppBar from 'substance/AppBar';
import Button from 'substance/Button';
import Icon from 'substance/Icon';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      AppBar(ToolButton(Icon('menu')), ToolButton('A textual toolbutton')),
      StyleManager()
    ];
  }
});
```
