# Button #

A `Button` provides and indicates an action to be taken to the user. The content of a `Button` is up to the designer,
allthough usually this is an [`Icon`](Icon), a text or both.

## Usage ##

`Button(attributes, children)`

### Arguments ###

attributes
:   Additional attributes to style the `Button` and add events.

    animation
    :   The animation to apply if the `Button` is clicked.

        Defaults to [`Ripple`](Ripple).


    textColor
    :   The color of the foreground text of the `Button`.

        No default.


    type
    :   The `Button` style. One of `Button.Type.Action`, `Button.Type.Flat` or `Button.Type.Raised`.

        Defaults to `Type.Action`.


    *event*
    :   Any event as specified by the [HTML button element](https://w3c.github.io/html/sec-forms.html#the-button-element>).

children
:   Child components or mithril vnodes. Can be written as splat arguments.

### Theming ###

backgroundColor
:   Sets the background color of the `Button`.

    Defaults to `palette.primary1Color`.


borderRadius
:   Specify the 'rounding' of the corners of the `Button`.

    Default to `'2px'`.


height
:   Set the height of the `Button`, based on the size of the device.

    Defaults to `{ small: '36px', medium: '31px', large: '55px' }`.


marginVertical
:   The vertical space between the `Button` and other elements.

    Defaults to `'6px'`.


paddingHorizontal
:   The horizontal space between the `Button` content and the border of the `Button`.

    Defaults to `{ small: '26px', medium: '16px', large: '26px' }`.


textColor
:   Sets the foreground color / text color used by default in the `Button`.

    Defaults to `palette.primaryTextColor`.


textSize
:   Set the size of text used by default in the `Button`.

    Defaults to `{ small: '13px', medium: '14px', large: '14px' }`.


## Examples ##

### Different kind of buttons ###

```javascript
import Button from 'substance/Button';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      Button('Default button'),
      StyleManager()
    ];
  }
});
```
