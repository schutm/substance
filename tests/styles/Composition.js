import Composition from '../../src/styles/Composition';

describe('Composition', () => {
	const composition = new Composition('bar_foo', 'bar', '.');

	it('selector returns new scoped CSS selector', () => {
		expect(composition.selector).toBe('.bar_foo');
	});

	it('unscopedSelector returns original CSS selector', () => {
		expect(composition.unscopedSelector).toBe('.bar');
	});

	it('explicit toString returns new scoped class name', () => {
		expect(composition.toString()).toBe('bar_foo');
	});

	it('implicit toString returns new scoped class name', () => {
		expect(`${composition}`).toBe('bar_foo');
	});
});
