import { withStyles } from './styles/withStyles';

function getLastScript() {
	const scripts = document.getElementsByTagName('script');

	// eslint-disable-next-line no-magic-numbers
	return scripts[scripts.length - 1];
}

function getScriptPath() {
	let src;

	try {
		src = (document.currentScript || getLastScript()).src;
	} catch (e) {
		src = '';
	}

	// eslint-disable-next-line no-magic-numbers
	return src.substring(0, Math.max(src.lastIndexOf('/'), src.lastIndexOf('\\')));
}

let fontPath = getScriptPath();

const stylesheet = () => `
	@font-face {
	  font-family: 'Material Icons';
	  font-style: normal;
	  font-weight: 400;
	  src: url(${fontPath}/MaterialIcons-Regular.eot); /* For IE6-8PATH */
	  src: local('Material Icons'),
	       local('MaterialIcons-Regular'),
	       url(${fontPath}/MaterialIcons-Regular.woff2) format('woff2'),
	       url(${fontPath}/MaterialIcons-Regular.woff) format('woff'),
	       url(${fontPath}/MaterialIcons-Regular.ttf) format('truetype');
	}

	.icon {
	  font-family: 'Material Icons';
	  font-weight: normal;
	  font-style: normal;
	  font-size: 24px;  /* Preferred icon size */
	  display: inline-block;
	  text-transform: none;
	  letter-spacing: normal;
	  word-wrap: normal;
	  white-space: nowrap;
	  direction: ltr;

		-webkit-font-smoothing: antialiased;
	  text-rendering: optimizeLegibility;
	  -moz-osx-font-smoothing: grayscale;

	  font-feature-settings: 'liga';
	}`;

const defaultTheme = ({ _palette }) => {
	return {};
};

export class Icon {
	view(styles, vnode) {
		return m(`i.${styles.icon}`, Object.assign({}, vnode.attrs), vnode.children);
	}

	static setFontPath(path) {
		fontPath = path;
	}

	static getFontPath() {
		return fontPath;
	}

	static createComponent(theme) {
		return withStyles(stylesheet, theme)(this);
	}
}

const component = Icon.createComponent(defaultTheme);
function IconComponent(...args) {
	return m(component, ...args);
}
IconComponent.setFontPath = Icon.setFontPath;
IconComponent.getFontPath = Icon.getFontPath;
export default IconComponent;
