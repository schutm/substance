/* eslint-disable no-magic-numbers */

import {
	cyan700,
	grey600,
	pinkA100, pinkA200, pinkA400,
	fullWhite, fullBlack
} from './colors';
import { fade } from '../utils/colorManipulator';

const theme = {
	fontFamily: 'Roboto, sans-serif',
	palette: {
		canvasColorLight: '#073642',
		canvasColorDark: '#002b36',

		primary1Color: cyan700,
		primary2Color: cyan700,
		primary3Color: grey600,
		accent1Color: pinkA200,
		accent2Color: pinkA400,
		accent3Color: pinkA100,
		primaryTextColor: fullWhite,
		secondaryTextColor: fade(fullWhite, 0.7),
		disabledTextColor: fade(fullWhite, 0.3),
		alternateTextColor: '#303030',

		borderColor: fade(fullWhite, 0.3),
		disabledColor: fade(fullWhite, 0.3),
		shadowColor: fullBlack
	}
};

export default theme;
