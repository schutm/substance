import { StyleManager } from '../../src';

import { withStyles } from '../../src/styles/withStyles';

import m from 'mithril/mithril';
global.m = m;

const defaultTheme = () => {
	return { };
};
const stylesheetTest1 = () => `.test { a: a }`;
const stylesheetTest2 = () => `.test { b: b }`;

const testComponent1 = withStyles(stylesheetTest1, defaultTheme)(class Test1 {
	view(styles, vnode) { return m(`.${styles.test}`, vnode.children); }
});

const testComponent2 = withStyles(stylesheetTest2, defaultTheme)(class Test2 {
	view(styles, vnode) { return m(`.${styles.test}`, vnode.children); }
});

function createComponents() {
	m.mount(window.document.body, {
		view() {
			return m('div', m(testComponent1), m(testComponent2), StyleManager());
		}
	});
}

test('StyleManager HTML structure renders correctly', () => {
	createComponents();

	expect(window.document.documentElement).toMatchSnapshot();
});
