import { activateTheme } from './chromic/chromic.js';

import AppBar from '../src/AppBar';
import Button, { Appearance as ButtonAppearance } from '../src/Button';
import ToolButton from '../src/ToolButton';
import Icon from '../src/Icon';
import StyleManager from '../src/StyleManager';

import light from '../src/themes/light';

activateTheme(light);

m.mount(document.body, {
	view(_vnode) {
		return m('.app', [
			m('.appbar', AppBar('The appbar is shown at the top')),
			m('.button', [
				['.regular', '.pressed'].map((state) =>
					m(state, Object.values(ButtonAppearance.Type).map((type) => Button({ type }, `${state}, ${type}`)))
				)
			]),
			m('.icon', Icon('menu')),
			m('.toolbutton', ToolButton('Toolbutton')),
			StyleManager()
		]);
	}
});
