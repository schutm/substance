export default {
	GridLayout: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center'
	},
	FillWidth: {
		flex: 1
	}
};
