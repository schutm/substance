import { Ripple, activateTheme } from '../../src';

import { createWrappedComponent, dispatch } from '../__helpers__/component';
import { themeLight } from '../__helpers__/themes';

test('Ripple animation events are emitted', async () => {
	activateTheme(themeLight);
	const { component, domNode } = createWrappedComponent(Ripple)({ hotspot: { x: 20, y: 20 } });

	let animationState = 'started';
	component.state.animationsFinished.then(
		() => { animationState = 'ended'; }
	);

	expect(animationState).toBe('started');

	await dispatch.transitionEvent(domNode, 'transitionend', 'width');
	expect(animationState).toBe('started');

	await dispatch.transitionEvent(domNode, 'transitionend', 'opacity');
	expect(animationState).toBe('ended');
});
