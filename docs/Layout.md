# Layout #

`Layout`s are snippets used to arrange items in the user interface. They're commonly mixed into the `style` object of
the `attributes` argument of components.


## Usage ##

If no other styles need to be applied to a component the `Layout`-snippet can be assigned directly to the
`style`-attribute as in: `Component({ style: Layout.GridLayout })`. If more `style`-attributes need to be set the
following construct can be used `Component({ style: Object.assign({ backgroundColor: 'red'}, Layout.FillWidth) })`.
This can be shortened, if your environment supports the object spread operator, to
`Component({  style: { backgroundColor: 'red', ...Layout.FillWidth }})`.


## Snippets ##

FillWidth
:    Indicates the component should take up as much space as possible.


GridLayout
:   Indicates the child components should be laid out as a horizontal grid.


## Examples ##

### Panel ###

```javascript
import Panel from 'substance/Panel';
import Layout from 'substance/Layout';
import light from 'substance/themes/light';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      Panel({ style: Layout.GridLayout},
        Panel({ style: { backgroundColor: 'red' }}, 'Left'),
        Panel({ style: Object.assign({
          backgroundColor: 'yellow',
          textAlign: 'center'
        }, Layout.FillWidth)}, 'Middle'),
        Panel({ style: { backgroundColor: 'blue' }}, 'Right')
      ),
      StyleManager()
    ];
  }
});
```
