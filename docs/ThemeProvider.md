# ThemeProvider #

Using the `ThemeProvider` it is possible to use different themes on a single page. All elements specified as children
of the `ThemeProvider` are using the new specified theme. The main theme can be set using either an initial
`ThemeProvider` or using the global `activateTheme` method.

## Usage ##

`ThemeProvider(attributes, children)`

### Arguments ###

attributes
:   Additional attributes.

    theme
    :   The theme to use for the child elements.


children
:   Child components or mithril vnodes. Can be written as splat arguments.

It is not possible to specify the exact contents of a theme as it differs per element and implementation of the element.


## Examples ##

### Different themings ###

```javascript
import Button from 'substance/Button';
import ThemeProvider from 'substance/ThemeProvider';
import light from 'substance/themes/light';
import dark from 'substance/themes/dark';

activateTheme(light);
m.mount(document.body, {
  view(_vnode) {
    return [
      m('div', {style: {background: '#ffffff'}}, Button('A light themed button')),
      m('div', {style: {background: '#000000'}}, ThemeProvider({ theme: dark }, Button('A dark themed button'))),
      StyleManager()
    ];
  }
});
```
