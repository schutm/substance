/**
 * Tests based on csjs module https://github.com/rtsao/csjs/
 */
import ThemedStylesheet from '../../src/styles/ThemedStylesheet';

function performAssertions(input) {
	const result = new ThemedStylesheet(input);

	it('unscoped css equals input', () => {
		expect(result.unscopedCss).toBe(input);
	});

	it('it keyframe mapping to be correct', () => {
		expect(result.keyframes).toMatchSnapshot();
	});

	it('it class mapping to be correct', () => {
		const classes = Object.keys(result.classes).reduce((acc, key) =>
			Object.assign({}, acc, { [key]: `${result.classes[key]}` }), {}
		);
		expect(classes).toMatchSnapshot();
	});

	it('it css property accesses the scoped CSS output to be correct', () => {
		expect(result.css).toMatchSnapshot();
	});

	it('explicit toString() accesses the scoped CSS output to be correct', () => {
		expect(result.toString()).toMatchSnapshot();
	});

	it('implicit toString() accesses the scoped CSS output to be correct', () => {
		expect(`${result}`).toMatchSnapshot();
	});
}

describe('ThemedStylesheet basic scoping', () => {
	const input = `
		.foo {
			color: red;
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet animations with pseudo-selector names scoping', () => {
	const input = `
		@keyframes hover {
			0%   { opacity: 0.0; }
			100% { opacity: 0.5; }
		}
		@media (max-width: 480px) {
			.animation:hover {
				background: green;
			}
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet with comments scoping', () => {
	const input = `
		/* Here's a comment */
		/*squishedcomment*/
		/********crazycomment********/
		/* A comment with a period at the end. */
		/* A comment with a period followed by numbers v1.2.3 */
		/* A comment with a .className */
		/*
		 * Here's a comment
		 * A comment with a period at the end.
		 * A comment with a period followed by numbers v1.2.3
		 * A comment with a .className
		 */
		/* .inlineCss { color: red; } */
		/*
		.commentedOutCss {
			color: blue;
		}
		*/
		.foo {
			color: red; /* comment on a line */
			animation-name: wow;
		}
		@keyframes wow {}
		/*
		@keyframes bam {}
		 */

		/* .woot {
			animation-name: bam;
		} */
		/* .hmm {
			animation-name: wow;
		} */`;

	performAssertions(input);
});

describe('ThemedStylesheet with dots in values scoping', () => {
	const input = `
		.foo {
			font-size: 1.3em;
		}

		.bar { font-size: 12.5px; } .baz { width: 33.3% }`;

	performAssertions(input);
});

describe('ThemedStylesheet with decimals in keyframes scoping', () => {
	const input = `
		@keyframes woot {
			0%   { opacity: 0; }
			33.3% { opacity: 0.333; }
			100% { opacity: 1; }
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet with media queries scoping', () => {
	const input = `
		.foo {
			color: red;
		}

		@media (max-width: 480px) {
			.foo {
				color: blue;
			}
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet with non-class selectors scoping', () => {
	const input = `
		#foo {
			color: red;
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet with not-pseudo selector scoping', () => {
	const input = `
		@media screen and (min-width: 769px) {
			.foo:not(.bar) {
				display: flex;
			}
		}`;

	performAssertions(input);
});

describe('ThemedStylesheet', () => {
	const style = new ThemedStylesheet(`.foo { color: red; } .bar { color: green; }`);
	const classes = style.classes;

	it('translates unscoped classname to scoped classname', () => {
		expect(classes('foo')).toBe('.foo_4k3Auy');
	});

	it('translates array correct', () => {
		expect(classes('foo', null, 'bar', undefined, 'selector')).toBe('.foo_4k3Auy .bar_4k3Auy .selector');
	});

	it('keeps unknown unscoped classname', () => {
		expect(classes('selector')).toBe('.selector');
	});

	it('removes undefined classname', () => {
		expect(classes(null)).toBe('');
	});
});
