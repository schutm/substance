/* eslint-disable no-magic-numbers */

import { contrastColor, fade } from '../../src/utils/colorManipulator';

describe('HEX fading', () => {
	it('result in a correct rgba value', () => {
		expect(fade('#FFFFFF', 0.5)).toBe('rgba(255, 255, 255, 0.5)');
	});

	it('result in a correct rgb value', () => {
		expect(fade('#FFFFFF', 1)).toBe('rgb(255, 255, 255)');
	});
});

describe('Shorthand HEX Fading', () => {
	it('result in a correct rgba value', () => {
		expect(fade('#FFF', 0.5)).toBe('rgba(255, 255, 255, 0.5)');
	});

	it('result in a correct rgb value', () => {
		expect(fade('#FFF', 1)).toBe('rgb(255, 255, 255)');
	});
});

describe('RGB fading', () => {
	it('result in a correct rgba value', () => {
		expect(fade('rgb(255,255,255)', 0.5)).toBe('rgba(255, 255, 255, 0.5)');
	});

	it('result in a correct rgb value', () => {
		expect(fade('rgb(255,255,255)', 1)).toBe('rgb(255, 255, 255)');
	});
});

describe('RGBA fading', () => {
	it('result in a correct rgba value', () => {
		expect(fade('rgba(255,255,255,.25)', 0.5)).toBe('rgba(255, 255, 255, 0.5)');
	});

	it('result in a correct rgb value', () => {
		expect(fade('rgba(255,255,255,.25)', 1)).toBe('rgb(255, 255, 255)');
	});
});

describe('HSL fading', () => {
	it('result in a correct hsla value', () => {
		expect(fade('hsl(120,1%,.25%)', 0.5)).toBe('hsla(120, 1%, 0.25%, 0.5)');
	});

	it('result in a correct hsl value', () => {
		expect(fade('hsl(120,1%,.25%)', 1)).toBe('hsl(120, 1%, 0.25%)');
	});
});

describe('HSLA fading', () => {
	it('result in a correct hsla value', () => {
		expect(fade('hsla(120,1%,.25%,.25)', 0.5)).toBe('hsla(120, 1%, 0.25%, 0.5)');
	});

	it('result in a correct hsl value', () => {
		expect(fade('hsla(120,1%,.25%,.25)', 1)).toBe('hsl(120, 1%, 0.25%)');
	});
});

describe('Clamping', () => {
	it('result in a correct minimum alpha of 0', () => {
		expect(fade('#FFF', -1)).toBe('rgba(255, 255, 255, 0)');
	});

	it('result in a correct maximum alpha of 1', () => {
		expect(fade('#FFF', 2)).toBe('rgb(255, 255, 255)');
	});
});

describe('RGB contrast color', () => {
	it('is the dark color with default threshold', () => {
		expect(contrastColor('#020202', 'light', 'dark')).toBe('dark');
	});

	it('is the  light color with default threshold', () => {
		expect(contrastColor('#DDD', 'light', 'dark')).toBe('light');
	});

	it('is the dark color with a very high threshold', () => {
		expect(contrastColor('#DDD', 'light', 'dark', 21)).toBe('dark');
	});

	it('is the light color with a very low threshold', () => {
		expect(contrastColor('#020202', 'light', 'dark', 1)).toBe('light');
	});
});

describe('HSL contrast color', () => {
	it('is the dark color with default threshold', () => {
		expect(contrastColor('hsl(120,1%,5%)', 'light', 'dark')).toBe('dark');
	});

	it('is the  light color with default threshold', () => {
		expect(contrastColor('hsl(120,1%,95%)', 'light', 'dark')).toBe('light');
	});

	it('is the dark color with a very high threshold', () => {
		expect(contrastColor('hsl(120,1%,95%)', 'light', 'dark', 21)).toBe('dark');
	});

	it('is the light color with a very low threshold', () => {
		expect(contrastColor('hsl(120,1%,5%)', 'light', 'dark', 1)).toBe('light');
	});
});
