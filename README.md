substance
=========
substance provides opiniated default UI components for [mithril components](http://mithril.js.org). The documentation is
[online available](https://schutm.gitlab.io/substance/docs).

*Most recent build*<br />
[![Commit](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/raw/.public/shields/commit.svg?job=prepare)](https://gitlab.com/schutm/substance/commits/master)
[![Pipeline status](https://gitlab.com/schutm/substance/badges/master/build.svg)](https://gitlab.com/schutm/substance/commits/master)
[![Tests](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/raw/.public/shields/tests.svg?job=test)](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/browse?job=test)
[![Coverage](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/raw/.public/shields/coverage.svg?job=test)](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/browse?job=test)
[![Style](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/raw/.public/shields/style.svg?job=lint)](https://gitlab.com/schutm/substance/-/jobs/artifacts/master/browse?job=lint)

*Last deployed build*<br />
[![Commit](https://schutm.gitlab.io/substance/shields/commit.svg)]()
[![Build](https://schutm.gitlab.io/substance/shields/build.svg)](https://schutm.gitlab.io/substance/substance.latest.tar.gz)
[![Tests](https://schutm.gitlab.io/substance/shields/tests.svg)](https://schutm.gitlab.io/substance/tests)
[![Coverage](https://schutm.gitlab.io/substance/shields/coverage.svg)](https://schutm.gitlab.io/substance/coverage)
[![Style](https://schutm.gitlab.io/substance/shields/style.svg)](https://schutm.gitlab.io/substance/lint)


Installation
------------
As you'll not I don't use [npm](https://npmjs.com) as it doesn't fit my
development process. This makes the installation a bit cumbersome. If
you create a proper `package.json` to submit this library to npm, I'll
happily [accept the pull-request](CONTRIBUTING.md).

1. Make sure you have to the latest version downloaded
   * [Download the latest release](https://github.com/schutm/substance/zipball/master).
   * Clone the repo:
     `git clone git://github.com/schutm/substance.git`.
2. Copy or move the relevant files from the `dist` directory to your own
   project.


Bug tracker
-----------
[Open a new issue](https://github.com/schutm/substance/issues) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).
